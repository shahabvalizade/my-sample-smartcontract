// choosing solidity version
pragma solidity ^0.5.4;

/* writing our first
smart contract code
to print helloworld message */

contract HelloWorld {
    string private helloMessage = "Hello World!";
    function getHelloMessage() public view returns (string memory) {
        return helloMessage;
    }
}